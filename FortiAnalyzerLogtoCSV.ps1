##########################################
## Name: FortiAnalyzerLogtoCSV.ps1
#
## Description:
# This script convert FortiAnalyzer Logfiles
# (exported from the FortiAnalyzer Logviewer
# download option to "log" to an Excel readable
# CSV file.
#
## Create Date: 03.04.2020
## Created by: Andy Scherer
#
## Last update date: never
## Last updated by: --
#
## Version: 1.0
#
##########################################
#--------------------
# Imports
#--------------------
Add-Type -AssemblyName System.Windows.Forms

#--------------------
# General Settings
# Global Variables
#--------------------
$regexFields = '(\w+)(?:=)(?:"{1,2}([\w\-\.:\ =]+)"{1,3})|(\w+)=(?:([\w\-\.:\=]+))' # Regex matches "field=value" or "field=""more words""" syntax
$events = @() # ArrayLists with all events
$headers = [System.Collections.ArrayList]@() #Array with all Headers


#--------------------
# Functions
#--------------------
function Save-File([string] $initialDirectory ) 
 
{
 
[System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
 
$OpenFileDialog = New-Object System.Windows.Forms.SaveFileDialog
$OpenFileDialog.initialDirectory = $initialDirectory
$OpenFileDialog.filter = "CSV (Excel) (*.csv)| *.csv"
$OpenFileDialog.ShowDialog() |  Out-Null
 
return $OpenFileDialog.filename
} 

#--------------------
# MAIN
#--------------------

$OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
	
if ($OpenFileDialog.ShowDialog() -ne "Cancel") {
    $LogFilePath = $OpenFileDialog.FileName # User select Log file from FileDialog
} else {
    throw "No File Selected"
}

Write-Output "[+] Reading logs from $($LogFilePath)"

foreach($line in Get-Content $LogFilePath) {
    $event = New-Object PSObject

    $matches = $line | Select-String -pattern $regexFields -AllMatches # Find all regex matches on each line
    
    foreach($group in $matches.Matches.Value) {
        # add a key,value pair to the object for each key=value group
        $groupSplit = $group -split '=' # split in key and value 
        $event| Add-Member -membertype NoteProperty -name $groupSplit[0]-value $groupSplit[1]
    }
    $events += $event # Add object to ArrayList
}


Write-Output "[+] Writing CSV"

$Newfilename = Save-File $LogFilePath
$events | Export-Csv $Newfilename -NoTypeInformation -Delimiter ";" # export arraylist to csv