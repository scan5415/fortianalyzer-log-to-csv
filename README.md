# FortiAnalyzer Log to CSV

## Converts fortigate log exports into CSV files
Fortigate logs export in a "field1=value","field2=value" format which isn't easily parsed
This script pulls out each field and compiles the events into a single CSV file

## Usage
Example: Export logs from Fortianalyzer as "csv"


![alt text](fac-download.png "FortiAuthenticator Download")

![alt text](run-script.png "Start Powershell script")